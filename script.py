import RPi.GPIO as GPIO
import signal
import sys
import time
import optparse

# CONSTANTS
PIN_COIN_INTERRUPT = 7
PIN_RELAY_INTERRUPT = 11

# INIT VARIABLES
lastImpulse = 0
pulses = 0
relayDown = GPIO.LOW
relayUp = GPIO.HIGH
smart = True
delay = 0

def coinEventHandler(pin):
  global smart
  global pulses
  global relayDown
  global lastImpulse
  lastImpulse = time.time()
  if ( pulses == 0 ) :
    if ( smart ):
      GPIO.output( PIN_RELAY_INTERRUPT, relayDown )
    print('coindrop')
    sys.stdout.flush()
  pulses = pulses + 1
  
def signal_handler(signal, frame):
  #global PIN_RELAY_INTERRUPT
  print('Exiting...')
  #GPIO.output(PIN_RELAY_INTERRUPT, GPIO.LOW)
  GPIO.cleanup()
  sys.exit(0)

def main():
  global smart
  global pulses
  global lastImpulse
  global relayDown
  global relayUp
  global delay
  global PIN_COIN_INTERRUPT
  global PIN_RELAY_INTERRUPT
  
  p = optparse.OptionParser()
  p.add_option('--coin', default=7)
  p.add_option('--state', default=0)
  p.add_option('--solenoid', default=11)
  p.add_option('--smart', default='yes')
  options, arguments = p.parse_args()
  try:
    PIN_RELAY_STATE = int(options.state)
    if ( PIN_RELAY_STATE == 1 ):
      relayDown = GPIO.HIGH
      relayUp = GPIO.LOW
  except:
    pass
  try:
    PIN_COIN_INTERRUPT = int(options.coin)
  except:
    pass
  try:
    PIN_RELAY_INTERRUPT = int(options.solenoid)
  except:
    pass
  try:
    smart = options.smart == 'yes'
  except:
    pass
  try:
    delay = int(options.delay)
  except:
    pass
  
  GPIO.setmode( GPIO.BOARD )
  GPIO.setup( PIN_COIN_INTERRUPT, GPIO.IN )
  GPIO.setup( PIN_RELAY_INTERRUPT, GPIO.OUT )
  GPIO.add_event_detect( PIN_COIN_INTERRUPT, GPIO.FALLING, callback=coinEventHandler, bouncetime=70 )
  GPIO.output(PIN_RELAY_INTERRUPT, relayUp)
  
  signal.signal( signal.SIGINT, signal_handler )
  if ( delay > 0):
    time.sleep(delay)
  print('ready')
  sys.stdout.flush()
  while True :
    time.sleep(0.1)

    if ( time.time() - lastImpulse > 0.135 ) and ( pulses > 0 ) :
      if ( smart ):
        GPIO.output(PIN_RELAY_INTERRUPT, relayUp)
      print(pulses)
      pulses = 0;
      sys.stdout.flush()

  GPIO.cleanup()

if __name__=="__main__":
  main()