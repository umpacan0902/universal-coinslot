'use strict';
Object.assign(String.prototype, {
  toHHMM() {
    if (this == '-') return this; 
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    //return hours+':'+minutes+':'+seconds;
    return hours+':'+minutes;
  }
});

Object.assign(String.prototype, {
  toSeconds() { 
    var seconds = 0;
    if (this == '-') return this; 
    if (!this) return 0; 
    if (this.indexOf(':')) {
      var hms = this.split(':'); 
      seconds = (+hms[0]) * 60 * 60 + (+hms[1]) * 60;
    } else if ( !isNaN(this) ){
      seconds = parseInt(this,10);
    }
    return seconds; 
  }
})

module.exports = (sequelize, DataTypes) => {
  const cointable = sequelize.define('cointable', {
    pulses: DataTypes.INTEGER,
    credits: DataTypes.INTEGER,
    time: {
      type: DataTypes.INTEGER,
      get() {
        const time = this.getDataValue('time') || 0;
        return time.toString().toHHMM();
      },
      set(time) {
        this.setDataValue('time', time.toString().toSeconds());
      },
    },
    expiry: {
      type: DataTypes.INTEGER,
      get() {
        const expiry = this.getDataValue('expiry') || "-";
        return expiry.toString().toHHMM();
      },
      set(expiry) {
        this.setDataValue('expiry', expiry.toString().toSeconds());
      },
    }
  }, {});
  cointable.initModel = db => {
    db['cointable'].sync({alter:true}).catch(e=>console.log(e));
  };
  return cointable;
};