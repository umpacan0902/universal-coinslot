
var path = require('path')

register_options = {
  'insert-coin-wait-time': 20,
  'insert-coin-penalty-time1': 10,
  'insert-coin-penalty-time2': 40,
  'insert-coin-distance': 70,
  'insert-coin-pin1': 7,
  'insert-coin-relay-pin': 11,
  'insert-coin-relay-state': 0,
  'insert-coin-relay-delay': 0,
  'insert-coin-smart': 'yes',
  'insert-coin-pulse-distribution': 'factor',
}

form_handler = function( ){
  if ( posted['coin-new'] || posted['coin-edit'] ) {
    var values = {
      pulses: posted.pulses,
      credits: posted.credits,
      time: posted.time,
      expiry: posted.expiry
    }
    if ( values.pulses && values.credits && values.time ) {
      return db.cointable.findOrCreate( { where: { pulses: { [db.Sequelize.Op.eq]: values.pulses } } , defaults: values } )
        .spread((coin, created) => { 
          if (!created) {
            coin.update( values, { where: { pulses: { [db.Sequelize.Op.eq]: values.pulses } } } );
          }
          return coin;
        });
    }
  }
  else if ( posted['coin-delete'] ) {
    return db.cointable.findOne({ where: { id: { [db.Sequelize.Op.eq]: posted['coin-delete'] } }})
      .then(result =>{ if (result) result.destroy(); })
  } 
}

data_store = function( next ){
  var data = {};
  Promise.all([
    db.cointable.findAll()
      .then(result=>{
        var coins = [];
        result.forEach(function(coin){ // coin is a db object and not raw, .get() is needed.
          coins.push(coin.get());
        });
        data.cointable = coins;
        //console.log(coins)
        return coins;
      }),
    get_option( 'insert-coin-wait-time' )
      .then(time=>{ data.waittime = time; return time;}),
    get_option( 'insert-coin-distance' )
      .then(distance=>{ data.distance = distance; return distance; }),
    get_option( 'insert-coin-pin1' )
      .then(pin1=>{ data.pin1 = pin1; return pin1; }),
    get_option( 'insert-coin-relay-pin' )
      .then(relaypin=>{ data.relaypin = relaypin; return relaypin; }),
    get_option( 'insert-coin-relay-state' )
      .then(relaystate=>{ data.relaystate = parseInt(relaystate,10); return relaystate; }),
    get_option( 'insert-coin-penalty-time1' )
      .then(penaltytime1=>{ data.penaltytime1 = penaltytime1; return penaltytime1; }),
    get_option( 'insert-coin-penalty-time2' )
      .then(penaltytime2=>{ data.penaltytime2 = penaltytime2; return penaltytime2; }),
    get_option( 'insert-coin-smart' )
      .then(smart=>{ data.smart = smart; return smart; }),
    get_option( 'insert-coin-pulse-distribution' )
      .then(pulse_distribution=>{ data.pulse_distribution = pulse_distribution; return pulse_distribution; })
  ])
  .then(result=>{ next(data) })
}

